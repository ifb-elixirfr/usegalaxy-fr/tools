pykwalify==1.7.0
bioblend==0.16.0

# These ones are from ephemeris, copied here to avoid installing useless pysam
six>=1.9.0
PyYAML
Jinja2
galaxy-tool-util>=20.9.1
galaxy-util>=20.9.0
